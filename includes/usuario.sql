CREATE TABLE usuario (
    nombre varchar(64),
    paterno varchar(64),
    materno varchar(64),
    correo varchar(128) primary key,
    telefono int,
    edad int,
    pass varchar(256),
    domicilio varchar(128)
);

