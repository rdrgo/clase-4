<?php include 'includes/conexion.php'; 

$correo = $_GET['user'];

$sql = "SELECT * FROM usuario WHERE correo='$correo'";
$resultado = $conexion->query($sql);
$resultado = $resultado->fetch_assoc();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <?php require 'extensiones/head.php' ?>
    <title>Modificar usuario</title>
</head>
<body style="background: #dfdfdf;">
    <?php require 'extensiones/navbar.php' ?>

    <div class="contenedor">
        <div class="titulo">
            <h3>Modificar usuario</h3>
            <hr>
        </div>
        <div class="cuerpo">
            <form action="editar-usuario.php" method="POST">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <span>Nombre:</span>
                            <input class="form-control" type="text" id="nombre" name="nombre" value="<?php echo $resultado['nombre'] ?>" required>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <span>Apellido Paterno:</span>
                            <input class="form-control" type="text" id="paterno" name="paterno" value="<?php echo $resultado['paterno'] ?>">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <span>Apellido Materno:</span>
                            <input class="form-control" type="text" id="materno" name="materno" value="<?php echo $resultado['materno'] ?>">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <span>Correo electrónico:</span>
                            <input class="form-control" type="email" id="correo" name="correo" value="<?php echo $resultado['correo'] ?>" readonly>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <span>Teléfono:</span>
                            <input class="form-control" type="text" id="telefono" name="telefono" value="<?php echo $resultado['telefono'] ?>">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <span>Edad:</span>
                            <input class="form-control" type="number" id="edad" name="edad" value="<?php echo $resultado['edad'] ?>">
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="form-group">
                            <span>Domicilio:</span>
                            <input class="form-control" type="text" id="domicilio" name="domicilio" value="<?php echo $resultado['domicilio'] ?>">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <button type="submit" class="btn btn-block" style="background: #597E54; color: white">Modificar</button>
                        </div>
                    </div>
                </div>
            </form>
            <div class="row">
                <div class="col-md-12">
                    <?php 
                        if(!empty($_GET['error'])){
                            $respuesta = $_GET['error'];
                            $contenido = $_GET['contenido'];
                    ?>
                        <?php   if($respuesta=='vacio'){ ?>
                                <div class="col-md-12">
                                    <div class="alert alert-success" role="alert">
                                        <?php echo $contenido ?>
                                    </div>
                                </div>
                        <?php   } ?>
                    <?php 
                       } 
                    ?>
                </div>
            </div>
        </div>
    </div>

    <?php require 'extensiones/scripts.php'?>
</body>
</html>