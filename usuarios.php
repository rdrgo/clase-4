<?php 

include 'includes/conexion.php';

$query = "SELECT * FROM usuario";
$consulta_usuarios = $conexion->query($query);


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <?php require 'extensiones/head.php'?>
    <title>Usuarios registrados</title>
</head>
<body style="background: #dfdfdf;">
    <?php require 'extensiones/navbar.php'?>

    <div class="contenedor">
        <div class="table-responsive" style="padding: 1%">
            <table class="table table-bordered" id="usuarios">
                <thead class="thead-light">
                    <tr>
                        <th scope="col">Nompre</th>
                        <th scope="col">Apellido Paterno</th>
                        <th scope="col">Apellido Materno</th>
                        <th scope="col">Correo</th>
                        <th scope="col">Edad</th>
                        <th scope="col">Opciones</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                if($consulta_usuarios->num_rows > 0){
                    while ($usuarios = $consulta_usuarios->fetch_assoc()){                 
                ?>
                    <tr>
                        <td><?php echo $usuarios['nombre']?></td>
                        <td><?php echo $usuarios['paterno']?></td>
                        <td><?php echo $usuarios['materno']?></td>
                        <td><?php echo $usuarios['correo']?></td>
                        <td><?php echo $usuarios['edad']?></td>
                        <td>
                            <div class="row">
                                <div class="col-md-6">
                                    <a href="modificar-usuario.php?user=<?php echo $usuarios['correo']?>" class="btn btn-block" style="background: #597E54; color: white">Modificar</a>
                                </div>
                                <div class="col-md-6">
                                    <a href="eliminar-usuario.php?user=<?php echo $usuarios['correo']?>" class="btn btn-block" style="background: #597E54; color: white">Eliminar</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                <?php }} ?>
                </tbody>
            </table>
        </div>
        <div class="row">
                <div class="col-md-12">
                    <?php 
                        if(!empty($_GET['error'])){
                            $respuesta = $_GET['error'];
                            $contenido = $_GET['contenido'];
                    ?>
                        <?php   if($respuesta=='modificado'){ ?>
                                <div class="col-md-12">
                                    <div class="alert alert-success" role="alert">
                                        <?php echo $contenido ?>
                                    </div>
                                </div>
                        <?php   } ?>
                    <?php 
                       } 
                    ?>
                </div>
            </div>
    </div>
    
    <?php require 'extensiones/scripts.php'?>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#usuarios').DataTable({
                language: {
                    search: "Buscar:",
                    paginate: {
                        first: "Primer",
                        previous: "Anterior",
                        next: "Siguiente",
                        last: "Último"
                    },
                    info: "Mostrando del _START_ al _END_ de _TOTAL_ resultados disponibles",
                    emptyTable: "No existen elementos para mostrar en la tabla",
                    infoEmpty: "Mostrando del 0 al 0 de 0 resultados",
                    infoFiltered: "(Filtrado de _MAX_ resultados)",
                    lengthMenu: "Mostrando _MENU_ resultados",
                    loadingRecords: "Cargando...",
                    processing: "Procesando...",
                    zeroRecords: "No se encontraron resultados",
                    aria: {
                        sortAscending: ": Ordenado de forma ascendente",
                        sortDescending: ": Ordenado de forma descendente"
                    }

                }
            });
        });
    </script>
</body>
</html>

